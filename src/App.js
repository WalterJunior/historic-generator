import React from "react";
import Navbar from "./components/Navbar";
import Historic from "./components/Historic";
import "./App.sass";

function App() {
  return (
    <>
      <Navbar />
      <div className="container" style={{ marginTop: 50 }}>
        <Historic />
      </div>
    </>
  );
}

export default App;
