import React, { useState } from "react";
import copy from "copy-to-clipboard";
import { Multiselect } from "react-widgets";
import projects from "../../data";

export default function Historic() {
  const [isLoading, setIsLoading] = useState(false);
  const [issue, setIssue] = useState("");
  const [historic, setHistoric] = useState("");
  const [result, setResult] = useState("");
  const [selectedProjects, setSelectedProjects] = useState([]);

  function generate() {
    let projectsResult = "";
    selectedProjects.forEach(p => (projectsResult += `-> ${p}\n`));
    setResult(`<h>${historic}</h>

== Code Review ==
Projeto(s):
${projectsResult}

Branch(es):
-> feature/${
      issue && issue.includes("id=")
        ? issue.substr(issue.lastIndexOf("id=") + 3, issue.length)
        : ""
    }

------------------------------

== Equipe QA ==
Wiki ->`);
    setHistoric("");
    setIssue("");
    setSelectedProjects([]);
  }

  function copyToClipboard() {
    setIsLoading(true);
    copy(result);
    setTimeout(() => {
      setIsLoading(false);
      setResult("");
    }, 2000);
  }

  return (
    <>
      <div className="columns is-mobile">
        <div className="column is-6 is-offset-one-quarter">
          <input
            className="input"
            type="text"
            placeholder="Histórico"
            value={historic}
            onChange={evt => setHistoric(evt.target.value)}
          />
        </div>
      </div>

      <div className="columns is-mobile">
        <div className="column is-6 is-offset-one-quarter">
          <input
            className="input"
            type="text"
            placeholder="URL do caso"
            value={issue}
            onChange={evt => setIssue(evt.target.value)}
          />
        </div>
      </div>

      <div className="columns is-mobile">
        <div className="column is-6 is-offset-one-quarter">
          <Multiselect
            data={projects}
            value={selectedProjects}
            onChange={selected => setSelectedProjects(selected)}
            filter="contains"
            placeholder="Selecione 1 ou mais projetos..."
            messages={{
              emptyList: "Todos os projetos já foram selecionados",
              emptyFilter: "Nenhum resultado foi encontrado"
            }}
          />
        </div>
      </div>

      <div className="columns is-mobile">
        <div className="column is-6 is-offset-one-quarter">
          <button
            className="button is-info is-outlined is-fullwidth"
            onClick={generate}
          >
            Gerar Histórico
          </button>
        </div>
      </div>

      <div className="columns is-mobile">
        <div className="column is-6 is-offset-one-quarter">
          <textarea
            rows="8"
            disabled
            className="textarea"
            style={{ resize: "none" }}
            value={result}
          />
        </div>
      </div>

      <div className="columns is-mobile">
        <div className="column is-6 is-offset-one-quarter">
          <button
            className={`button is-success is-outlined is-fullwidth ${
              isLoading ? "is-loading" : ""
            }`}
            onClick={copyToClipboard}
          >
            Copiar
          </button>
        </div>
      </div>
    </>
  );
}
