import React from "react";

export default function Navbar() {
  return (
    <section className="hero is-dark">
      <div className="hero-body">
        <div className="container">
          <nav className="level">
            <div className="level-item has-text-centered">
              <h2 className="title">Gerador de Históricos</h2>
            </div>
          </nav>
        </div>
      </div>
    </section>
  );
}
